// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBlcuCn7fiTBTcZpkR0gkfNHlktajOOke4',
    authDomain: 'oshop-52aef.firebaseapp.com',
    databaseURL: 'https://oshop-52aef.firebaseio.com',
    projectId: 'oshop-52aef',
    storageBucket: 'oshop-52aef.appspot.com',
    messagingSenderId: '983504827743'
  }
};
